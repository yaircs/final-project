export interface IAuthenticationQueryRes {
  agent_id: number;
  secret_key: string;
}
